# twiliogram
A Telegram bot interface to Twilio.

## Install
```sh
git clone https://gitlab.com/lazlowcarmichael/twiliogram.git
cd twiliogram
npm install
```

## Configure
Add your Twilio and Telegram details to index.js:

```js
const cfg = {
  twilio: {
    accountSid: '<ACCOUNT_SID>',
    authToken: '<AUTH_TOKEN>',
    sendingNumber: '<SENDING_NUMBER>'
  },
  tgram: {
    token: '<TELEGRAM_TOKEN>'
  }
};
```

## Usage
Start twiliogram:

```sh
npm start
```

Send your Telegram bot the ```/sms``` command:

```sh
sms [1112223333] [message]
```

## Support the Author
If you enjoy ```twiliogram``` and would like to help me continue as a developer, you can show your support to me and my favorite community by delegating to my validator node, [Classic49](https://explorer.unification.io/validator/undvaloper1hxjv5xlf8rn6elgcu6vu90wff22zvs2lnq4sya), on the [Unification](https://unification.com) network.

Of course direct ETH donations are accepted at ```0xf1d7f40ad9479664c964f1E10939F068A9247965```
