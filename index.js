const cfg = {
  twilio: {
    accountSid: '',
    authToken: '',
    sendingNumber: ''
  },
  tgram: {
    token: ''
  }
};

const requiredTwilio = [
  cfg.twilio.accountSid,
  cfg.twilio.authToken,
  cfg.twilio.sendingNumber
];

const requiredTgram = [ cfg.tgram.token ];

let twilioConfigured = requiredTwilio.every((configValue) => {
  return configValue || false;
});

let tgramConfigured = requiredTgram.every((configValue) => {
  return configValue || false;
});

if (!twilioConfigured || !tgramConfigured) {
  const error = '[!] FATAL: Check configuration.';
  throw new Error(error);
}

const twilio = require('twilio')(cfg.twilio.accountSid, cfg.twilio.authToken);

const TelegramBot = require('node-telegram-bot-api');
const telegram = new TelegramBot(cfg.tgram.token, { polling: true });

telegram.onText(/\/sms ([0-9]{10}) (.*){1,200}/, (msg, matches) => {
  const chat_id = msg.chat.id;

  twilio.api.messages
    .create({
      body: matches[2],
      from: cfg.twilio.sendingNumber,
      to: matches[1]
    })
    .then(() => { console.log('[*] Message forwarded.'); })
    .catch((error) => {
      console.error(`[!] An error has occured:\n${error}`);
    });
});
